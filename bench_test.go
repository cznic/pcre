// Copyright 2018 The pcre Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE file.

package pcre

import (
	"regexp"
	"runtime/debug"
	"testing"

	"github.com/glenn-brown/golang-pkg-pcre/src/pkg/pcre"
)

// Modified code from https://benchmarksgame-team.pages.debian.net/benchmarksgame/program/regexredux-go-1.html

/* The Computer Language Benchmarks Game
 * https://salsa.debian.org/benchmarksgame-team/benchmarksgame/
 *
 * regex-dna program contributed by The Go Authors.
 * converted from regex-dna program
 */

var (
	variants = []string{
		"agggtaaa|tttaccct",
		"[cgt]gggtaaa|tttaccc[acg]",
		"a[act]ggtaaa|tttacc[agt]t",
		"ag[act]gtaaa|tttac[agt]ct",
		"agg[act]taaa|ttta[agt]cct",
		"aggg[acg]aaa|ttt[cgt]ccct",
		"agggt[cgt]aa|tt[acg]accct",
		"agggta[cgt]a|t[acg]taccct",
		"agggtaa[cgt]|[acg]ttaccct",
	}

	substs = []Subst{
		{"tHa[Nt]", "<4>"},
		{"aND|caN|Ha[DS]|WaS", "<3>"},
		{"a[NSt]|BY", "<2>"},
		{"<[^>]*>", "|"},
		{"\\|[^|][^|]*\\|", "-"},
	}
)

type Subst struct {
	pat, repl string
}

func countMatchesGoCC(pat string, bytes []byte) (int, error) {
	re := MustCompile(pat, 0, Table{})
	n := 0
	for {
		e, err := re.FindIndex(bytes, 0)
		if err != nil {
			return 0, err
		}

		if e == nil {
			return n, nil
		}

		n++
		bytes = bytes[e[1]:]
	}
}

func BenchmarkRegexReduxGoCC(b *testing.B) {
	var ilen, clen, result int
	var out []int
	var err error
	debug.FreeOSMemory()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf := append([]byte(nil), shortTestData...)
		ilen = len(buf)
		// Delete the comment lines and newlines
		if buf, err = MustCompile("(>[^\n]+)?\n", 0, Table{}).ReplaceAll(buf, nil, 0); err != nil {
			b.Fatal(err)
		}
		clen = len(buf)

		mresults := make([]chan int, len(variants))
		for i, s := range variants {
			ch := make(chan int)
			mresults[i] = ch
			go func(ss string) {
				n, err := countMatchesGoCC(ss, buf)
				if err != nil {
					b.Error(err)
				}

				ch <- n
			}(s)
		}

		lenresult := make(chan int)
		bb := buf
		go func() {
			for _, sub := range substs {
				bb, err = MustCompile(sub.pat, 0, Table{}).ReplaceAll(bb, []byte(sub.repl), 0)
				if err != nil {
					b.Error(err)
				}
			}
			lenresult <- len(bb)
		}()

		out = out[:0]
		for i := range variants {
			out = append(out, <-mresults[i])
		}
		result = <-lenresult
	}
	b.StopTimer()
	b.SetBytes(int64(ilen))
	results := shortTestResults
	for i, e := range results.e {
		if g := out[i]; g != e {
			b.Error(i, g, e)
		}
	}
	if g, e := ilen, results.ilen; g != e {
		b.Error(g, e)
	}

	if g, e := clen, results.clen; g != e {
		b.Error(g, e)
	}

	if g, e := result, results.result; g != e {
		b.Error(g, e)
	}
}

func countMatchesGo(pat string, bytes []byte) int {
	re := regexp.MustCompile(pat)
	n := 0
	for {
		e := re.FindIndex(bytes)
		if e == nil {
			break
		}
		n++
		bytes = bytes[e[1]:]
	}
	return n
}

func BenchmarkRegexReduxGo(b *testing.B) {
	var ilen, clen, result int
	var out []int
	debug.FreeOSMemory()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf := append([]byte(nil), shortTestData...)
		ilen = len(buf)
		// Delete the comment lines and newlines
		buf = regexp.MustCompile("(>[^\n]+)?\n").ReplaceAll(buf, []byte{})
		clen = len(buf)

		mresults := make([]chan int, len(variants))
		for i, s := range variants {
			ch := make(chan int)
			mresults[i] = ch
			go func(ss string) {
				ch <- countMatchesGo(ss, buf)
			}(s)
		}

		lenresult := make(chan int)
		bb := buf
		go func() {
			for _, sub := range substs {
				bb = regexp.MustCompile(sub.pat).ReplaceAll(bb, []byte(sub.repl))
			}
			lenresult <- len(bb)
		}()

		out = out[:0]
		for i := range variants {
			out = append(out, <-mresults[i])
		}
		result = <-lenresult
	}
	b.StopTimer()
	b.SetBytes(int64(ilen))
	results := shortTestResults
	for i, e := range results.e {
		if g := out[i]; g != e {
			b.Error(i, g, e)
		}
	}
	if g, e := ilen, results.ilen; g != e {
		b.Error(g, e)
	}

	if g, e := clen, results.clen; g != e {
		b.Error(g, e)
	}

	if g, e := result, results.result; g != e {
		b.Error(g, e)
	}
}

// Modified code from https://benchmarksgame-team.pages.debian.net/benchmarksgame/program/regexredux-go-2.html

/* The Computer Language Benchmarks Game
 * https://salsa.debian.org/benchmarksgame-team/benchmarksgame/
 *
 * regex-dna program contributed by The Go Authors.
 * modified by Tylor Arndt.
 * modified by Chandra Sekar S to use optimized PCRE binding.
 * converted from regex-dna program
 */

func countMatchesCGo(pat string, bytes []byte) int {
	re := pcre.MustCompile(pat, 0)
	n := 0
	for {
		e := re.FindIndex(bytes, 0)
		if e == nil {
			break
		}
		n++
		bytes = bytes[e[1]:]
	}
	return n
}

func BenchmarkRegexReduxCGo(b *testing.B) {
	var ilen, clen, result int
	var out []int
	debug.FreeOSMemory()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf := append([]byte(nil), shortTestData...)
		ilen = len(buf)
		// Delete the comment lines and newlines
		buf = pcre.MustCompile("(>[^\n]+)?\n", 0).ReplaceAll(buf, []byte{}, 0)
		clen = len(buf)

		mresults := make([]chan int, len(variants))
		var i int
		var s string
		for i, s = range variants {
			ch := make(chan int)
			mresults[i] = ch
			go func(intch chan int, ss string) {
				intch <- countMatchesCGo(ss, buf)
			}(ch, s)
		}

		lenresult := make(chan int)
		bb := buf
		go func() {
			for _, sub := range substs {
				bb = pcre.MustCompile(sub.pat, 0).ReplaceAll(bb, []byte(sub.repl), 0)
			}
			lenresult <- len(bb)
		}()

		out = out[:0]
		for i = range variants {
			out = append(out, <-mresults[i])
		}
		result = <-lenresult
	}
	b.StopTimer()
	b.SetBytes(int64(ilen))
	results := shortTestResults
	for i, e := range results.e {
		if g := out[i]; g != e {
			b.Error(i, g, e)
		}
	}
	if g, e := ilen, results.ilen; g != e {
		b.Error(g, e)
	}

	if g, e := clen, results.clen; g != e {
		b.Error(g, e)
	}

	if g, e := result, results.result; g != e {
		b.Error(g, e)
	}
}
