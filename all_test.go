// Copyright 2018 The pcre Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE file.

//go:generate go run generator.go

package pcre

import (
	"fmt"
	"os"
	"os/exec"
	"path"
	"regexp"
	"runtime"
	"strings"
	"testing"

	// "github.com/dustin/go-humanize"
	"github.com/glenn-brown/golang-pkg-pcre/src/pkg/pcre"
	// "modernc.org/crt/v2"
)

func caller(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(2)
	fmt.Fprintf(os.Stderr, "# caller: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	_, fn, fl, _ = runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# \tcallee: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func dbg(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# dbg %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func TODO(...interface{}) string { //TODOOK
	_, fn, fl, _ := runtime.Caller(1)
	return fmt.Sprintf("# TODO: %s:%d:\n", path.Base(fn), fl) //TODOOK
}

func use(...interface{}) {}

func init() {
	use(caller, dbg, TODO) //TODOOK
}

// ============================================================================

// ==== jnml@4670:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . |& tee log-bench-20190203-2221
// Sun Feb  3 22:21:24 CET 2019
// go version devel +4b3f04c63b Thu Jan 10 18:15:48 2019 +0000 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxCCGo-4   	       1	33608229065 ns/op	   1.51 MB/s
// BenchmarkRegexReduxGo-4     	       1	19262143799 ns/op	   2.64 MB/s
// BenchmarkRegexReduxCGo-4    	       1	14307463450 ns/op	   3.55 MB/s
// PASS
// ok  	modernc.org/pcre	67.216s

// ==== jnml@e5-1650:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . |& tee log-bench-20190205-1026-master
// Tue Feb  5 10:26:06 CET 2019
// go version go1.11.4 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxCCGo-12    	       1	47810614984 ns/op	   1.06 MB/s
// BenchmarkRegexReduxGo-12      	       1	22697464961 ns/op	   2.24 MB/s
// BenchmarkRegexReduxCGo-12     	       1	12521619542 ns/op	   4.06 MB/s
// PASS
// ok  	modernc.org/pcre	83.052s

// ==== jnml@e5-1650:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench-20190205-1232-no_defer
// Tue Feb  5 12:32:46 CET 2019
// go version go1.11.4 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxCCGo-12    	       1	21197271023 ns/op	   2.40 MB/s	1185759296 B/op	   21101 allocs/op
// BenchmarkRegexReduxGo-12      	       1	21881277246 ns/op	   2.32 MB/s	1186046440 B/op	   21841 allocs/op
// BenchmarkRegexReduxCGo-12     	       1	12018179068 ns/op	   4.23 MB/s	1188782760 B/op	  105022 allocs/op
// PASS
// ok  	modernc.org/pcre	55.119s

// ==== jnml@e5-1650:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench-20190205-1232-no_stack_recursion
// Tue Feb  5 12:46:27 CET 2019
// go version go1.11.4 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxCCGo-12    	       1	20415903842 ns/op	   2.49 MB/s	1185755392 B/op	   21092 allocs/op
// BenchmarkRegexReduxGo-12      	       1	22261952355 ns/op	   2.28 MB/s	1186046248 B/op	   21842 allocs/op
// BenchmarkRegexReduxCGo-12     	       1	12373212799 ns/op	   4.11 MB/s	1188782920 B/op	  105024 allocs/op
// PASS
// ok  	modernc.org/pcre	55.074s

// ==== jnml@e5-1650:~/src/modernc.org/pcre> date ; go version ; go test -v -timeout 24h -bench . -benchmem |& tee log-bench-20190206-1211
// Wed Feb  6 12:11:38 CET 2019
// go version go1.11.4 linux/amd64
// === RUN   Test
// --- PASS: Test (0.00s)
// === RUN   TestRegexReduxCCGo
// --- PASS: TestRegexReduxCCGo (0.36s)
// === RUN   TestRegexReduxGo
// --- PASS: TestRegexReduxGo (0.27s)
// === RUN   TestRegexReduxCGo
// --- PASS: TestRegexReduxCGo (0.26s)
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxCCGo-12    	       1	21021233883 ns/op	   2.42 MB/s	1185756128 B/op	   21092 allocs/op
// BenchmarkRegexReduxGo-12      	       1	22797355540 ns/op	   2.23 MB/s	1186046856 B/op	   21840 allocs/op
// BenchmarkRegexReduxCGo-12     	       1	11882054322 ns/op	   4.28 MB/s	1188786840 B/op	  105028 allocs/op
// PASS
// ok  	modernc.org/pcre	56.609s

// ==== jnml@e5-1650:~/src/modernc.org/pcre> date ; go version ; go test -v -timeout 24h -bench . -benchmem |& tee log-bench-20190206-1755-volatile_lhs
// Wed Feb  6 17:55:09 CET 2019
// go version go1.11.4 linux/amd64
// === RUN   Test
// --- PASS: Test (0.00s)
// === RUN   TestRegexReduxCCGo
// --- PASS: TestRegexReduxCCGo (0.32s)
// === RUN   TestRegexReduxGo
// --- PASS: TestRegexReduxGo (0.24s)
// === RUN   TestRegexReduxCGo
// --- PASS: TestRegexReduxCGo (0.25s)
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxCCGo-12    	       1	20738173769 ns/op	   2.45 MB/s	1185754400 B/op	   21090 allocs/op
// BenchmarkRegexReduxGo-12      	       1	23498174727 ns/op	   2.16 MB/s	1186047144 B/op	   21843 allocs/op
// BenchmarkRegexReduxCGo-12     	       1	12191125285 ns/op	   4.17 MB/s	1188781816 B/op	  105017 allocs/op
// PASS
// ok  	modernc.org/pcre	57.261s

// ==== jnml@4670:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench-20190209-1037
// So úno  9 10:37:00 CET 2019
// go version devel +4b3f04c63b Thu Jan 10 18:15:48 2019 +0000 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxCCGo-4   	       1	22343490813 ns/op	   2.28 MB/s	1185755040 B/op	   21102 allocs/op
// BenchmarkRegexReduxGo-4     	       1	20646530528 ns/op	   2.46 MB/s	1186029224 B/op	   21911 allocs/op
// BenchmarkRegexReduxCGo-4    	       1	14638987537 ns/op	   3.47 MB/s	1186444472 B/op	   84116 allocs/op
// PASS
// ok  	modernc.org/pcre	57.662s

// ==== jnml@4670:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench-20190209-2219
// So úno  9 22:19:37 CET 2019
// go version devel +4b3f04c63b Thu Jan 10 18:15:48 2019 +0000 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxCCGo-4   	       1	21917260913 ns/op	   2.32 MB/s	1185755312 B/op	   21101 allocs/op
// BenchmarkRegexReduxGo-4     	       1	19262521536 ns/op	   2.64 MB/s	1186065720 B/op	   21909 allocs/op
// BenchmarkRegexReduxCGo-4    	       1	14290577487 ns/op	   3.56 MB/s	1186439672 B/op	   84107 allocs/op
// PASS
// ok  	modernc.org/pcre	55.499s

// ==== jnml@e5-1650:~/src/modernc.org/pcre> go test -v -bench . -benchmem -short |& tee log
// === RUN   Test
// --- PASS: Test (0.00s)
// === RUN   TestRegexReduxGoCC
//     TestRegexReduxGoCC: all_test.go:230:
// --- SKIP: TestRegexReduxGoCC (0.00s)
// === RUN   TestRegexReduxGoCCShort
// --- PASS: TestRegexReduxGoCCShort (8.16s)
// === RUN   TestRegexReduxGo
//     TestRegexReduxGo: all_test.go:302:
// --- SKIP: TestRegexReduxGo (0.00s)
// === RUN   TestRegexReduxGoShort
// --- PASS: TestRegexReduxGoShort (0.25s)
// === RUN   TestRegexReduxCGo
//     TestRegexReduxCGo: all_test.go:363:
// --- SKIP: TestRegexReduxCGo (0.00s)
// === RUN   TestRegexReduxCGoShort
// --- PASS: TestRegexReduxCGoShort (0.27s)
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxGoCC
// BenchmarkRegexReduxGoCC-12    	       1	7964188656 ns/op	   0.13 MB/s	12638048 B/op	   16527 allocs/op
// BenchmarkRegexReduxGo
// BenchmarkRegexReduxGo-12      	       5	 215703556 ns/op	   4.64 MB/s	11911388 B/op	     540 allocs/op
// BenchmarkRegexReduxCGo
// BenchmarkRegexReduxCGo-12     	       5	 244887453 ns/op	   4.08 MB/s	12374785 B/op	   16612 allocs/op
// PASS
// ok  	modernc.org/pcre	23.682s

// jnml@4670:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench-20200425-1156
// So dub 25 11:56:45 CEST 2020
// go version go1.14.2 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxGoCC-4   	       1	9081889944 ns/op	   0.11 MB/s	12638784 B/op	   16530 allocs/op
// BenchmarkRegexReduxGo-4     	       5	 228488984 ns/op	   4.38 MB/s	11909259 B/op	     538 allocs/op
// BenchmarkRegexReduxCGo-4    	       4	 253982806 ns/op	   3.94 MB/s	12375120 B/op	   16614 allocs/op
// PASS
// ok  	modernc.org/pcre	14.273s
// jnml@4670:~/src/modernc.org/pcre>

//  4	BenchmarkRegexReduxGoCC-4   	       1	9135181366 ns/op	   0.11 MB/s	12638880 B/op	   16531 allocs/op
//  5	BenchmarkRegexReduxGoCC-4   	       1	9048531713 ns/op	   0.11 MB/s	12638688 B/op	   16529 allocs/op
//  6	BenchmarkRegexReduxGoCC-4   	       1	9081196484 ns/op	   0.11 MB/s	12639552 B/op	   16541 allocs/op
//  7	BenchmarkRegexReduxGoCC-4   	       1	9078334968 ns/op	   0.11 MB/s	12638592 B/op	   16528 allocs/op
//  8	BenchmarkRegexReduxGoCC-4   	       1	9078318404 ns/op	   0.11 MB/s	12639072 B/op	   16533 allocs/op
//  9	BenchmarkRegexReduxGoCC-4   	       1	9017052772 ns/op	   0.11 MB/s	12639264 B/op	   16530 allocs/op
// 10	BenchmarkRegexReduxGoCC-4   	       1	9107251754 ns/op	   0.11 MB/s	12638592 B/op	   16528 allocs/op
// 11	BenchmarkRegexReduxGoCC-4   	       1	2346166227 ns/op	   0.43 MB/s	12638688 B/op	   16529 allocs/op
// 12	BenchmarkRegexReduxGoCC-4   	       1	1591572851 ns/op	   0.63 MB/s	12638208 B/op	   16527 allocs/op
// 13	BenchmarkRegexReduxGoCC-4   	       1	1584055587 ns/op	   0.63 MB/s	12639072 B/op	   16533 allocs/op

// jnml@4670:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench-20200425-1420_4k_stack_segment
// So dub 25 14:21:12 CEST 2020
// go version go1.14.2 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxGoCC-4   	       1	1657557059 ns/op	   0.60 MB/s	12638784 B/op	   16530 allocs/op
// BenchmarkRegexReduxGo-4     	       5	 260294992 ns/op	   3.84 MB/s	11908822 B/op	     536 allocs/op
// BenchmarkRegexReduxCGo-4    	       4	 256851884 ns/op	   3.89 MB/s	12374760 B/op	   16612 allocs/op
// PASS
// ok  	modernc.org/pcre	7.848s
// jnml@4670:~/src/modernc.org/pcre>

// jnml@4670:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench-20200425-1711_4k_private_stack_segment
// So dub 25 17:11:44 CEST 2020
// go version go1.14.2 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxGoCC-4   	       1	1583469565 ns/op	   0.63 MB/s	43385760 B/op	   49376 allocs/op
// BenchmarkRegexReduxGo-4     	       5	 227582751 ns/op	   4.39 MB/s	11907900 B/op	     538 allocs/op
// BenchmarkRegexReduxCGo-4    	       4	 257413890 ns/op	   3.88 MB/s	12374064 B/op	   16611 allocs/op
// PASS
// ok  	modernc.org/pcre	6.778s

// jnml@4670:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench
// So dub 25 17:30:37 CEST 2020
// go version go1.14.2 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxGoCC-4   	       1	1554379783 ns/op	   0.64 MB/s	11881248 B/op	     156 allocs/op
// BenchmarkRegexReduxGo-4     	       5	 239707256 ns/op	   4.17 MB/s	11907478 B/op	     536 allocs/op
// BenchmarkRegexReduxCGo-4    	       5	 259605269 ns/op	   3.85 MB/s	12375092 B/op	   16613 allocs/op
// PASS
// ok  	modernc.org/pcre	8.091s

//  4	BenchmarkRegexReduxGoCC-4   	       1	2839213283 ns/op	   0.35 MB/s	11881152 B/op	     154 allocs/op
//  5	BenchmarkRegexReduxGoCC-4   	       1	2870491418 ns/op	   0.35 MB/s	11880288 B/op	     152 allocs/op
//  6	BenchmarkRegexReduxGoCC-4   	       1	2909346176 ns/op	   0.34 MB/s	11880384 B/op	     147 allocs/op
//  7	BenchmarkRegexReduxGoCC-4   	       1	2881553808 ns/op	   0.35 MB/s	11883360 B/op	     153 allocs/op
//  8	BenchmarkRegexReduxGoCC-4   	       1	2895886757 ns/op	   0.35 MB/s	11884512 B/op	     151 allocs/op
//  9	BenchmarkRegexReduxGoCC-4   	       1	2862857351 ns/op	   0.35 MB/s	11880864 B/op	     152 allocs/op
// 10	BenchmarkRegexReduxGoCC-4   	       1	2787829524 ns/op	   0.36 MB/s	11880960 B/op	     153 allocs/op
// 11	BenchmarkRegexReduxGoCC-4   	       1	2378452875 ns/op	   0.42 MB/s	11881152 B/op	     155 allocs/op
// 12	BenchmarkRegexReduxGoCC-4   	       1	1579559002 ns/op	   0.63 MB/s	11881152 B/op	     155 allocs/op
// 13	BenchmarkRegexReduxGoCC-4   	       1	1647534881 ns/op	   0.61 MB/s	11881056 B/op	     154 allocs/op

// jnml@4670:~/src/modernc.org/pcre> date ; go version ; go test -timeout 24h -run @ -bench . -benchmem |& tee log-bench-20200425-1744_4k_stack_segment
// So dub 25 17:44:23 CEST 2020
// go version go1.14.2 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre
// BenchmarkRegexReduxGoCC-4   	       1	1614362773 ns/op	   0.62 MB/s	11851152 B/op	     121 allocs/op
// BenchmarkRegexReduxGo-4     	       5	 233370178 ns/op	   4.29 MB/s	11909494 B/op	     541 allocs/op
// BenchmarkRegexReduxCGo-4    	       4	 260806174 ns/op	   3.83 MB/s	12375144 B/op	   16614 allocs/op
// PASS
// ok  	modernc.org/pcre	7.528s

type results struct {
	e                  []int
	ilen, clen, result int
}

var (
	testData         []byte
	shortTestData    []byte
	testResults      = &results{[]int{356, 1250, 4252, 2894, 5435, 1537, 1431, 1608, 2178}, 50833411, 50000000, 27388361}
	shortTestResults = &results{[]int{0, 0, 0, 0, 0, 0, 0, 0, 0}, 1000000, 983585, 983585}
)

func init() {
	var err error
	if testData, err = exec.Command("go", "run", "fasta.go", "5000000").Output(); err != nil {
		panic(err)
	}

	shortTestData = testData[:1e6]
}

func Test(t *testing.T) {
	for iTest, v := range []struct {
		re, subj string
		m        []int32
	}{
		{"(a+)[^a]+(a+)[^a]+(a+)", "AaBaaCaaaD", []int32{1, 9, 1, 2, 3, 5, 6, 9}},
		//                          0123456789
	} {
		func() {
			re, err := Compile(v.re, 0, Table{})
			if err != nil {
				t.Fatal(iTest, err)
			}

			defer re.Close()

			var m [30]int32
			n, err := re.Exec([]byte(v.subj), 0, m[:])
			if err != nil {
				t.Fatal(iTest, err)
			}

			if g, e := 2*n, len(v.m); g != e {
				t.Fatalf("%v %v %v", iTest, m[:g], v.m)
			}

			for i, g := range m[:2*n] {
				if e := v.m[i]; g != e {
					t.Fatal(iTest, m[:2*n], v.m)
				}
			}
		}()
	}
}

func TestRegexReduxGoCC(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	testRegexReduxGoCC(t, append([]byte(nil), testData...), testResults)
}

func TestRegexReduxGoCCShort(t *testing.T) {
	testRegexReduxGoCC(t, append([]byte(nil), testData[:1e6]...), shortTestResults)
}

// $ go test -v -short -run GoCC
//
// Stack segment log
//  4	Locks: 53,278,522, Unlocks 53,278,522, SumMalloc 18,739,953,078, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs 26,622,792, StackFreeFrees 26,622,792
//  5	Locks: 53,278,522, Unlocks 53,278,522, SumMalloc 18,739,953,078, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs 26,622,792, StackFreeFrees 26,622,792
//  6	Locks: 53,278,522, Unlocks 53,278,522, SumMalloc 18,739,956,198, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs 26,622,792, StackFreeFrees 26,622,792
//  7	Locks: 53,278,440, Unlocks 53,278,440, SumMalloc 18,739,966,390, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs 26,622,751, StackFreeFrees 26,622,751
//  8	Locks: 53,278,370, Unlocks 53,278,370, SumMalloc 18,739,991,462, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs 26,622,716, StackFreeFrees 26,622,716
//  9	Locks: 53,278,364, Unlocks 53,278,364, SumMalloc 18,740,053,414, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs 26,622,713, StackFreeFrees 26,622,713
// 10	Locks: 53,278,028, Unlocks 53,278,028, SumMalloc 27,261,546,326, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs 26,622,545, StackFreeFrees 26,622,545
// 11	Locks:  2,065,988, Unlocks  2,065,988, SumMalloc  2,081,888,086, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs  1,016,525, StackFreeFrees  1,016,525
// 12	Locks:     66,040, Unlocks     66,040, SumMalloc     67,807,062, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs     16,551, StackFreeFrees     16,551
// 14	Locks:     65,816, Unlocks     65,816, SumMalloc    134,670,934, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs     16,439, StackFreeFrees     16,439
// 15	Locks:     65,816, Unlocks     65,816, SumMalloc    538,675,798, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs     16,439, StackFreeFrees     16,439

// Private stacks
// 12	Locks:     32,938, Unlocks     32,938, SumMalloc          2,646, StackAllocs 26,622,792, StackFrees 26,622,792, StackAllocAllocs     16,551, StackFreeFrees     16,551
func testRegexReduxGoCC(t *testing.T, buf []byte, results *results) {
	//TODO- crt.Locks = 0
	//TODO- crt.Unlocks = 0
	//TODO- crt.SumMalloc = 0
	//TODO- crt.StackAllocs = 0
	//TODO- crt.StackFrees = 0
	//TODO- crt.StackAllocAllocs = 0
	//TODO- crt.StackFreeFrees = 0

	//TODO- defer func() {
	//TODO- 	t.Logf("Locks: %10v, Unlocks %10v, SumMalloc %14v, StackAllocs %10v, StackFrees %10v, StackAllocAllocs %10v, StackFreeFrees %10v",
	//TODO- 		humanize.Comma(crt.Locks), humanize.Comma(crt.Unlocks),
	//TODO- 		humanize.Comma(crt.SumMalloc), humanize.Comma(crt.StackAllocs),
	//TODO- 		humanize.Comma(crt.StackFrees), humanize.Comma(crt.StackAllocAllocs),
	//TODO- 		humanize.Comma(crt.StackFreeFrees),
	//TODO- 	)
	//TODO- }()

	var ilen, clen, result int
	var out []int
	var err error
	ilen = len(buf)
	// Delete the comment lines and newlines
	if buf, err = MustCompile("(>[^\n]+)?\n", 0, Table{}).ReplaceAll(buf, nil, 0); err != nil {
		t.Fatal(err)
	}
	clen = len(buf)

	mresults := make([]chan int, len(variants))
	for i, s := range variants {
		ch := make(chan int)
		mresults[i] = ch
		go func(ss string) {
			n, err := countMatchesGoCC(ss, buf)
			if err != nil {
				t.Error(err)
			}

			ch <- n
		}(s)
	}

	lenresult := make(chan int)
	bb := buf
	go func() {
		for _, sub := range substs {
			bb, err = MustCompile(sub.pat, 0, Table{}).ReplaceAll(bb, []byte(sub.repl), 0)
			if err != nil {
				t.Error(err)
			}
		}
		lenresult <- len(bb)
	}()

	out = out[:0]
	for i := range variants {
		out = append(out, <-mresults[i])
	}
	result = <-lenresult
	for i, e := range results.e {
		if g := out[i]; g != e {
			t.Error(i, g, e)
		}
	}
	if g, e := ilen, results.ilen; g != e {
		t.Error(g, e)
	}

	if g, e := clen, results.clen; g != e {
		t.Error(g, e)
	}

	if g, e := result, results.result; g != e {
		t.Error(g, e)
	}
}

func TestRegexReduxGo(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	testRegexReduxGo(t, append([]byte(nil), testData...), testResults)
}

func TestRegexReduxGoShort(t *testing.T) {
	testRegexReduxGo(t, append([]byte(nil), testData[:1e6]...), shortTestResults)
}

func testRegexReduxGo(t *testing.T, buf []byte, results *results) {
	var ilen, clen, result int
	var out []int
	ilen = len(buf)
	// Delete the comment lines and newlines
	buf = regexp.MustCompile("(>[^\n]+)?\n").ReplaceAll(buf, []byte{})
	clen = len(buf)

	mresults := make([]chan int, len(variants))
	for i, s := range variants {
		ch := make(chan int)
		mresults[i] = ch
		go func(ss string) {
			ch <- countMatchesGo(ss, buf)
		}(s)
	}

	lenresult := make(chan int)
	bb := buf
	go func() {
		for _, sub := range substs {
			bb = regexp.MustCompile(sub.pat).ReplaceAll(bb, []byte(sub.repl))
		}
		lenresult <- len(bb)
	}()

	out = out[:0]
	for i := range variants {
		out = append(out, <-mresults[i])
	}
	result = <-lenresult
	for i, e := range results.e {
		if g := out[i]; g != e {
			t.Error(i, g, e)
		}
	}
	if g, e := ilen, results.ilen; g != e {
		t.Error(g, e)
	}

	if g, e := clen, results.clen; g != e {
		t.Error(g, e)
	}

	if g, e := result, results.result; g != e {
		t.Error(g, e)
	}
}

func TestRegexReduxCGo(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	testRegexReduxCGo(t, append([]byte(nil), testData...), testResults)
}

func TestRegexReduxCGoShort(t *testing.T) {
	testRegexReduxCGo(t, append([]byte(nil), testData[:1e6]...), shortTestResults)
}

func testRegexReduxCGo(t *testing.T, buf []byte, results *results) {
	var ilen, clen, result int
	var out []int
	ilen = len(buf)
	// Delete the comment lines and newlines
	buf = pcre.MustCompile("(>[^\n]+)?\n", 0).ReplaceAll(buf, []byte{}, 0)
	clen = len(buf)

	mresults := make([]chan int, len(variants))
	var i int
	var s string
	for i, s = range variants {
		ch := make(chan int)
		mresults[i] = ch
		go func(intch chan int, ss string) {
			intch <- countMatchesCGo(ss, buf)
		}(ch, s)
	}

	lenresult := make(chan int)
	bb := buf
	go func() {
		for _, sub := range substs {
			bb = pcre.MustCompile(sub.pat, 0).ReplaceAll(bb, []byte(sub.repl), 0)
		}
		lenresult <- len(bb)
	}()

	out = out[:0]
	for i = range variants {
		out = append(out, <-mresults[i])
	}
	result = <-lenresult
	for i, e := range results.e {
		if g := out[i]; g != e {
			t.Error(i, g, e)
		}
	}
	if g, e := ilen, results.ilen; g != e {
		t.Error(g, e)
	}

	if g, e := clen, results.clen; g != e {
		t.Error(g, e)
	}

	if g, e := result, results.result; g != e {
		t.Error(g, e)
	}
}
