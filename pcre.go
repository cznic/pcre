// Copyright 2018 The pcre Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE file.

// Copyright the Perl Compatible Regular Expressions authors. All right reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE-PCRE file.

// Package pcre is a set of functions that implement regular expression pattern
// matching using the same syntax and semantics as Perl, with just a few
// differences. Some features that appeared in Python and PCRE before they
// appeared in Perl are also available using the Python syntax, there is some
// support for one or two .NET and Oniguruma syntax items, and there is an
// option for requesting some minor changes that give better JavaScript
// compatibility.
package pcre

import (
	"errors"
	"fmt"
	"math"
	"unsafe"

	"modernc.org/crt/v2"
	"modernc.org/pcre/internal/pcre"
)

var (
	//		pcreFree = func(p uintptr) func(crt.TLS, uintptr) { return *(*func(crt.TLS, uintptr))(unsafe.Pointer(&p)) }(pcre.Xpcre_free)

	pcreErrors = map[int32]error{
		pcre.DPCRE_ERROR_BADCOUNT:       errors.New("bad count"),
		pcre.DPCRE_ERROR_BADENDIANNESS:  errors.New("bad endianness"),
		pcre.DPCRE_ERROR_BADLENGTH:      errors.New("bad length"),
		pcre.DPCRE_ERROR_BADMAGIC:       errors.New("bad magic"),
		pcre.DPCRE_ERROR_BADMODE:        errors.New("bad mode"),
		pcre.DPCRE_ERROR_BADNEWLINE:     errors.New("bad newline"),
		pcre.DPCRE_ERROR_BADOFFSET:      errors.New("bad offset"),
		pcre.DPCRE_ERROR_BADOPTION:      errors.New("bad option"),
		pcre.DPCRE_ERROR_BADPARTIAL:     errors.New("bad partial"),
		pcre.DPCRE_ERROR_BADUTF8:        errors.New("bad utf8"),
		pcre.DPCRE_ERROR_BADUTF8_OFFSET: errors.New("bad utf8 offset"),
		pcre.DPCRE_ERROR_CALLOUT:        errors.New("callout"),
		pcre.DPCRE_ERROR_DFA_BADRESTART: errors.New("dfa bad restart"),
		pcre.DPCRE_ERROR_DFA_RECURSE:    errors.New("dfa recurse"),
		pcre.DPCRE_ERROR_DFA_UCOND:      errors.New("dfa u cond"),
		pcre.DPCRE_ERROR_DFA_UITEM:      errors.New("dfa u item"),
		pcre.DPCRE_ERROR_DFA_UMLIMIT:    errors.New("dfa um limit"),
		pcre.DPCRE_ERROR_DFA_WSSIZE:     errors.New("dfa wssize"),
		pcre.DPCRE_ERROR_INTERNAL:       errors.New("internal"),
		pcre.DPCRE_ERROR_JIT_BADOPTION:  errors.New("jit bad option"),
		pcre.DPCRE_ERROR_JIT_STACKLIMIT: errors.New("jit stack limit"),
		pcre.DPCRE_ERROR_MATCHLIMIT:     errors.New("match limit"),
		pcre.DPCRE_ERROR_NOMATCH:        errors.New("no match"),
		pcre.DPCRE_ERROR_NOMEMORY:       errors.New("no memory"),
		pcre.DPCRE_ERROR_NOSUBSTRING:    errors.New("no substring"),
		pcre.DPCRE_ERROR_NULL:           errors.New("null"),
		pcre.DPCRE_ERROR_NULLWSLIMIT:    errors.New("null ws limit"),
		pcre.DPCRE_ERROR_PARTIAL:        errors.New("partial"),
		pcre.DPCRE_ERROR_RECURSELOOP:    errors.New("recurse loop"),
		pcre.DPCRE_ERROR_RECURSIONLIMIT: errors.New("recursion limit"),
		pcre.DPCRE_ERROR_SHORTUTF8:      errors.New("short utf8"),
		//TODO this or DPCRE_ERROR_UNKNOWN_OPCODE?
		// pcre.DPCRE_ERROR_UNKNOWN_NODE:   errors.New("unknown node"),
		pcre.DPCRE_ERROR_UNKNOWN_OPCODE: errors.New("unknown opcode"),
		pcre.DPCRE_ERROR_UNSET:          errors.New("unset"),
	}
)

type Option int

type Table struct{ crt.Intptr }

type Regexp struct {
	tls *crt.TLS
	re  crt.Intptr
}

// COMPILING A PATTERN
//
//        pcre *pcre_compile(const char *pattern, int options,
//             const char **errptr, int *erroffset,
//             const unsigned char *tableptr);
//
//        pcre *pcre_compile2(const char *pattern, int options,
//             int *errorcodeptr,
//             const char **errptr, int *erroffset,
//             const unsigned char *tableptr);
//
//        Either of the functions pcre_compile() or pcre_compile2() can be called
//        to compile a pattern into an internal form. The only difference between
//        the two interfaces is that pcre_compile2() has an additional  argument,
//        errorcodeptr,  via  which  a  numerical  error code can be returned. To
//        avoid too much repetition, we refer just to pcre_compile()  below,  but
//        the information applies equally to pcre_compile2().
//
//        The pattern is a C string terminated by a binary zero, and is passed in
//        the pattern argument. A pointer to a single block  of  memory  that  is
//        obtained  via  pcre_malloc is returned. This contains the compiled code
//        and related data. The pcre type is defined for the returned block; this
//        is a typedef for a structure whose contents are not externally defined.
//        It is up to the caller to free the memory (via pcre_free) when it is no
//        longer required.
//
//        Although  the compiled code of a PCRE regex is relocatable, that is, it
//        does not depend on memory location, the complete pcre data block is not
//        fully  relocatable, because it may contain a copy of the tableptr argu-
//        ment, which is an address (see below).
//
//        The options argument contains various bit settings that affect the com-
//        pilation.  It  should be zero if no options are required. The available
//        options are described below. Some of them (in  particular,  those  that
//        are  compatible with Perl, but some others as well) can also be set and
//        unset from within the pattern (see  the  detailed  description  in  the
//        pcrepattern  documentation). For those options that can be different in
//        different parts of the pattern, the contents of  the  options  argument
//        specifies their settings at the start of compilation and execution. The
//        PCRE_ANCHORED, PCRE_BSR_xxx, PCRE_NEWLINE_xxx, PCRE_NO_UTF8_CHECK,  and
//        PCRE_NO_START_OPTIMIZE  options  can  be set at the time of matching as
//        well as at compile time.
//
//        If errptr is NULL, pcre_compile() returns NULL immediately.  Otherwise,
//        if  compilation  of  a  pattern fails, pcre_compile() returns NULL, and
//        sets the variable pointed to by errptr to point to a textual error mes-
//        sage. This is a static string that is part of the library. You must not
//        try to free it. Normally, the offset from the start of the  pattern  to
//        the data unit that was being processed when the error was discovered is
//        placed in the variable pointed to by erroffset, which must not be  NULL
//        (if  it is, an immediate error is given). However, for an invalid UTF-8
//        or UTF-16 string, the offset is that of the  first  data  unit  of  the
//        failing character.
//
//        Some  errors are not detected until the whole pattern has been scanned;
//        in these cases, the offset passed back is the length  of  the  pattern.
//        Note  that  the  offset is in data units, not characters, even in a UTF
//        mode. It may sometimes point into the middle of a UTF-8 or UTF-16 char-
//        acter.
//
//        If  pcre_compile2()  is  used instead of pcre_compile(), and the error-
//        codeptr argument is not NULL, a non-zero error code number is  returned
//        via  this argument in the event of an error. This is in addition to the
//        textual error message. Error codes and messages are listed below.
//
//        If the final argument, tableptr, is NULL, PCRE uses a  default  set  of
//        character  tables  that  are  built  when  PCRE  is compiled, using the
//        default C locale. Otherwise, tableptr must be an address  that  is  the
//        result  of  a  call to pcre_maketables(). This value is stored with the
//        compiled pattern, and used again  by  pcre_exec()  and  pcre_dfa_exec()
//        when  the  pattern  is matched. For more discussion, see the section on
//        locale support below.
//
//        This code fragment shows a typical straightforward  call  to  pcre_com-
//        pile():
//
//          pcre *re;
//          const char *error;
//          int erroffset;
//          re = pcre_compile(
//            "^A.*Z",          /* the pattern */
//            0,                /* default options */
//            &error,           /* for error message */
//            &erroffset,       /* for error offset */
//            NULL);            /* use default character tables */

func Compile(pattern string, options Option, tables Table) (*Regexp, error) {
	// pcre *pcre_compile(const char *pattern, int options,
	//      const char **errptr, int *erroffset,
	//      const unsigned char *tableptr);

	//TODO pcre_study
	type out struct {
		errptr    crt.Intptr
		erroffset int32
	}

	tls := crt.NewTLS()
	cPattern := mustCString(pattern)
	co := mustMalloc(tls, unsafe.Sizeof(out{}))
	o := (*out)(unsafe.Pointer(uintptr(co)))

	defer func() {
		crt.Xfree(tls, cPattern)
		crt.Xfree(tls, co)
	}()

	r := pcre.Xpcre_compile(tls, cPattern, int32(options), crt.Intptr(uintptr(unsafe.Pointer(&o.errptr))), crt.Intptr(uintptr(unsafe.Pointer(&o.erroffset))), tables.Intptr)
	if r == 0 {
		return nil, fmt.Errorf("offset %v: %s", o.erroffset, crt.GoString(o.errptr))
	}

	return &Regexp{tls: tls, re: r}, nil
}

func (re *Regexp) Close() {
	tls := re.tls
	crt.Xfree(tls, re.re)
	tls.Close()
}

func mustMalloc(tls *crt.TLS, n uintptr) crt.Intptr {
	p := crt.Xmalloc(tls, crt.Intptr(n))
	if p == 0 {
		panic("OOM")
	}

	return p
}

func mustCString(s string) crt.Intptr {
	p, err := crt.CString(s)
	if err != nil {
		panic(err)
	}

	return p
}

// MustCompile is like Compile but panics if the expression cannot be parsed.
// It simplifies safe initialization of global variables holding compiled regular
// expressions.
func MustCompile(s string, options Option, tables Table) *Regexp {
	r, err := Compile(s, options, tables)
	if err != nil {
		panic(fmt.Errorf("pcre: Compile(%q): %v", s, err))
	}

	return r
}

//
//	// MATCHING A PATTERN: THE TRADITIONAL FUNCTION
//	//
//	//        int pcre_exec(const pcre *code, const pcre_extra *extra,
//	//             const char *subject, int length, int startoffset,
//	//             int options, int *ovector, int ovecsize);
//	//
//	//        The  function pcre_exec() is called to match a subject string against a
//	//        compiled pattern, which is passed in the code argument. If the  pattern
//	//        was  studied,  the  result  of  the study should be passed in the extra
//	//        argument. You can call pcre_exec() with the same code and  extra  argu-
//	//        ments  as  many  times as you like, in order to match different subject
//	//        strings with the same pattern.
//	//
//	//        This function is the main matching facility  of  the  library,  and  it
//	//        operates  in  a  Perl-like  manner. For specialist use there is also an
//	//        alternative matching function, which is described below in the  section
//	//        about the pcre_dfa_exec() function.
//	//
//	//        In  most applications, the pattern will have been compiled (and option-
//	//        ally studied) in the same process that calls pcre_exec().  However,  it
//	//        is possible to save compiled patterns and study data, and then use them
//	//        later in different processes, possibly even on different hosts.  For  a
//	//        discussion about this, see the pcreprecompile documentation.
//	//
//	//        Here is an example of a simple call to pcre_exec():
//	//
//	//          int rc;
//	//          int ovector[30];
//	//          rc = pcre_exec(
//	//            re,             /* result of pcre_compile() */
//	//            NULL,           /* we didn't study the pattern */
//	//            "some string",  /* the subject string */
//	//            11,             /* the length of the subject string */
//	//            0,              /* start at offset 0 in the subject */
//	//            0,              /* default options */
//	//            ovector,        /* vector of integers for substring information */
//	//            30);            /* number of elements (NOT size in bytes) */
//	//
//	//    Extra data for pcre_exec()
//	//
//	//        If  the  extra argument is not NULL, it must point to a pcre_extra data
//	//        block. The pcre_study() function returns such a block (when it  doesn't
//	//        return  NULL), but you can also create one for yourself, and pass addi-
//	//        tional information in it. The pcre_extra block contains  the  following
//	//        fields (not necessarily in this order):
//	//
//	//          unsigned long int flags;
//	//          void *study_data;
//	//          void *executable_jit;
//	//          unsigned long int match_limit;
//	//          unsigned long int match_limit_recursion;
//	//          void *callout_data;
//	//          const unsigned char *tables;
//	//          unsigned char **mark;
//	//
//	//        In  the  16-bit  version  of  this  structure,  the mark field has type
//	//        "PCRE_UCHAR16 **".
//	//
//	//        In the 32-bit version of  this  structure,  the  mark  field  has  type
//	//        "PCRE_UCHAR32 **".
//	//
//	//        The  flags  field is used to specify which of the other fields are set.
//	//        The flag bits are:
//	//
//	//          PCRE_EXTRA_CALLOUT_DATA
//	//          PCRE_EXTRA_EXECUTABLE_JIT
//	//          PCRE_EXTRA_MARK
//	//          PCRE_EXTRA_MATCH_LIMIT
//	//          PCRE_EXTRA_MATCH_LIMIT_RECURSION
//	//          PCRE_EXTRA_STUDY_DATA
//	//          PCRE_EXTRA_TABLES
//	//
//	//        Other flag bits should be set to zero. The study_data field  and  some-
//	//        times  the executable_jit field are set in the pcre_extra block that is
//	//        returned by pcre_study(), together with the appropriate flag bits.  You
//	//        should  not set these yourself, but you may add to the block by setting
//	//        other fields and their corresponding flag bits.
//	//
//	//        The match_limit field provides a means of preventing PCRE from using up
//	//        a  vast amount of resources when running patterns that are not going to
//	//        match, but which have a very large number  of  possibilities  in  their
//	//        search  trees. The classic example is a pattern that uses nested unlim-
//	//        ited repeats.
//	//
//	//        Internally, pcre_exec() uses a function called match(), which it  calls
//	//        repeatedly  (sometimes  recursively).  The  limit set by match_limit is
//	//        imposed on the number of times this function is called during a  match,
//	//        which  has  the  effect of limiting the amount of backtracking that can
//	//        take place. For patterns that are not anchored, the count restarts from
//	//        zero for each position in the subject string.
//	//
//	//        When pcre_exec() is called with a pattern that was successfully studied
//	//        with a JIT option, the way that the matching is  executed  is  entirely
//	//        different.  However, there is still the possibility of runaway matching
//	//        that goes on for a very long time, and so the match_limit value is also
//	//        used in this case (but in a different way) to limit how long the match-
//	//        ing can continue.
//	//
//	//        The default value for the limit can be set  when  PCRE  is  built;  the
//	//        default  default  is 10 million, which handles all but the most extreme
//	//        cases. You can override the default  by  suppling  pcre_exec()  with  a
//	//        pcre_extra     block    in    which    match_limit    is    set,    and
//	//        PCRE_EXTRA_MATCH_LIMIT is set in the  flags  field.  If  the  limit  is
//	//        exceeded, pcre_exec() returns PCRE_ERROR_MATCHLIMIT.
//	//
//	//        A  value  for  the  match  limit may also be supplied by an item at the
//	//        start of a pattern of the form
//	//
//	//          (*LIMIT_MATCH=d)
//	//
//	//        where d is a decimal number. However, such a setting is ignored  unless
//	//        d  is  less  than  the limit set by the caller of pcre_exec() or, if no
//	//        such limit is set, less than the default.
//	//
//	//        The match_limit_recursion field is similar to match_limit, but  instead
//	//        of limiting the total number of times that match() is called, it limits
//	//        the depth of recursion. The recursion depth is a  smaller  number  than
//	//        the  total number of calls, because not all calls to match() are recur-
//	//        sive.  This limit is of use only if it is set smaller than match_limit.
//	//
//	//        Limiting the recursion depth limits the amount of  machine  stack  that
//	//        can  be used, or, when PCRE has been compiled to use memory on the heap
//	//        instead of the stack, the amount of heap memory that can be used.  This
//	//        limit  is not relevant, and is ignored, when matching is done using JIT
//	//        compiled code.
//	//
//	//        The default value for match_limit_recursion can be  set  when  PCRE  is
//	//        built;  the  default  default  is  the  same  value  as the default for
//	//        match_limit. You can override the default by suppling pcre_exec()  with
//	//        a   pcre_extra   block  in  which  match_limit_recursion  is  set,  and
//	//        PCRE_EXTRA_MATCH_LIMIT_RECURSION is set in  the  flags  field.  If  the
//	//        limit is exceeded, pcre_exec() returns PCRE_ERROR_RECURSIONLIMIT.
//	//
//	//        A  value for the recursion limit may also be supplied by an item at the
//	//        start of a pattern of the form
//	//
//	//          (*LIMIT_RECURSION=d)
//	//
//	//        where d is a decimal number. However, such a setting is ignored  unless
//	//        d  is  less  than  the limit set by the caller of pcre_exec() or, if no
//	//        such limit is set, less than the default.
//	//
//	//        The callout_data field is used in conjunction with the  "callout"  fea-
//	//        ture, and is described in the pcrecallout documentation.
//	//
//	//        The  tables field is provided for use with patterns that have been pre-
//	//        compiled using custom character tables, saved to disc or elsewhere, and
//	//        then  reloaded,  because the tables that were used to compile a pattern
//	//        are not saved with it. See the pcreprecompile documentation for a  dis-
//	//        cussion  of  saving  compiled patterns for later use. If NULL is passed
//	//        using this mechanism, it forces PCRE's internal tables to be used.
//	//
//	//        Warning: The tables that pcre_exec() uses must be  the  same  as  those
//	//        that  were used when the pattern was compiled. If this is not the case,
//	//        the behaviour of pcre_exec() is undefined. Therefore, when a pattern is
//	//        compiled  and  matched  in the same process, this field should never be
//	//        set. In this (the most common) case, the correct table pointer is auto-
//	//        matically  passed  with  the  compiled  pattern  from pcre_compile() to
//	//        pcre_exec().
//	//
//	//        If PCRE_EXTRA_MARK is set in the flags field, the mark  field  must  be
//	//        set  to point to a suitable variable. If the pattern contains any back-
//	//        tracking control verbs such as (*MARK:NAME), and the execution ends  up
//	//        with  a  name  to  pass back, a pointer to the name string (zero termi-
//	//        nated) is placed in the variable pointed to  by  the  mark  field.  The
//	//        names  are  within  the  compiled pattern; if you wish to retain such a
//	//        name you must copy it before freeing the memory of a compiled  pattern.
//	//        If  there  is no name to pass back, the variable pointed to by the mark
//	//        field is set to NULL. For details of the  backtracking  control  verbs,
//	//        see the section entitled "Backtracking control" in the pcrepattern doc-
//	//        umentation.

func (re *Regexp) Exec(subject []byte, options Option, out []int32) (int, error) {
	// int pcre_exec(const pcre *code, const pcre_extra *extra,
	//      const char *subject, int length, int startoffset,
	//      int options, int *ovector, int ovecsize);

	if len(subject) > math.MaxInt32 {
		return 0, fmt.Errorf("%T.Exec: subject too long", re)
	}

	var o, s crt.Intptr
	if len(out) != 0 {
		o = crt.Intptr(uintptr(unsafe.Pointer(&out[0]))) //TODO wrong
	}
	if len(subject) != 0 {
		s = crt.Intptr(uintptr(unsafe.Pointer(&subject[0]))) //TODO wrong
	}
	tls := re.tls

	switch rc := pcre.Xpcre_exec(tls, re.re, 0 /*TODO study */, s, int32(len(subject)), 0, int32(options), o, int32(len(out))); {
	case rc < 0:
		if rc == pcre.DPCRE_ERROR_NOMATCH {
			return 0, nil
		}

		return 0, re.error(rc)
	case rc == 0:
		return 0, fmt.Errorf("%T.Exec: out has room for only %v captured substrings", re, len(out)/3-1)
	default:
		return int(rc), nil
	}
}

//	// INFORMATION ABOUT A PATTERN
//	//
//	//        int pcre_fullinfo(const pcre *code, const pcre_extra *extra,
//	//             int what, void *where);
//	//
//	//        The  pcre_fullinfo() function returns information about a compiled pat-
//	//        tern. It replaces the pcre_info() function, which was removed from  the
//	//        library at version 8.30, after more than 10 years of obsolescence.
//	//
//	//        The  first  argument  for  pcre_fullinfo() is a pointer to the compiled
//	//        pattern. The second argument is the result of pcre_study(), or NULL  if
//	//        the  pattern  was not studied. The third argument specifies which piece
//	//        of information is required, and the fourth argument is a pointer  to  a
//	//        variable  to  receive  the  data. The yield of the function is zero for
//	//        success, or one of the following negative numbers:
//	//
//	//          PCRE_ERROR_NULL           the argument code was NULL
//	//                                    the argument where was NULL
//	//          PCRE_ERROR_BADMAGIC       the "magic number" was not found
//	//          PCRE_ERROR_BADENDIANNESS  the pattern was compiled with different
//	//                                    endianness
//	//          PCRE_ERROR_BADOPTION      the value of what was invalid
//	//          PCRE_ERROR_UNSET          the requested field is not set
//	//
//	//        The "magic number" is placed at the start of each compiled  pattern  as
//	//        an  simple check against passing an arbitrary memory pointer. The endi-
//	//        anness error can occur if a compiled pattern is saved and reloaded on a
//	//        different  host.  Here  is a typical call of pcre_fullinfo(), to obtain
//	//        the length of the compiled pattern:
//	//
//	//          int rc;
//	//          size_t length;
//	//          rc = pcre_fullinfo(
//	//            re,               /* result of pcre_compile() */
//	//            sd,               /* result of pcre_study(), or NULL */
//	//            PCRE_INFO_SIZE,   /* what is required */
//	//            &length);         /* where to put the data */
//	//
//	//        The possible values for the third argument are defined in  pcre.h,  and
//	//        are as follows:
//	//
//	//          PCRE_INFO_BACKREFMAX
//	//
//	//        Return  the  number  of  the highest back reference in the pattern. The
//	//        fourth argument should point to an int variable. Zero  is  returned  if
//	//        there are no back references.
//	//
//	//          PCRE_INFO_CAPTURECOUNT
//	//
//	//        Return  the  number of capturing subpatterns in the pattern. The fourth
//	//        argument should point to an int variable.
//	//
//	//          PCRE_INFO_DEFAULT_TABLES
//	//
//	//        Return a pointer to the internal default character tables within  PCRE.
//	//        The  fourth  argument should point to an unsigned char * variable. This
//	//        information call is provided for internal use by the pcre_study() func-
//	//        tion.  External  callers  can  cause PCRE to use its internal tables by
//	//        passing a NULL table pointer.
//	//
//	//          PCRE_INFO_FIRSTBYTE (deprecated)
//	//
//	//        Return information about the first data unit of any matched string, for
//	//        a  non-anchored  pattern.  The  name of this option refers to the 8-bit
//	//        library, where data units are bytes. The fourth argument  should  point
//	//        to  an  int  variable. Negative values are used for special cases. How-
//	//        ever, this means that when the 32-bit library is  in  non-UTF-32  mode,
//	//        the  full  32-bit range of characters cannot be returned. For this rea-
//	//        son, this value is deprecated;  use  PCRE_INFO_FIRSTCHARACTERFLAGS  and
//	//        PCRE_INFO_FIRSTCHARACTER instead.
//	//
//	//        If  there  is  a  fixed first value, for example, the letter "c" from a
//	//        pattern such as (cat|cow|coyote), its value is returned. In  the  8-bit
//	//        library,  the  value is always less than 256. In the 16-bit library the
//	//        value can be up to 0xffff. In the 32-bit library the value can be up to
//	//        0x10ffff.
//	//
//	//        If there is no fixed first value, and if either
//	//
//	//        (a)  the pattern was compiled with the PCRE_MULTILINE option, and every
//	//        branch starts with "^", or
//	//
//	//        (b) every branch of the pattern starts with ".*" and PCRE_DOTALL is not
//	//        set (if it were set, the pattern would be anchored),
//	//
//	//        -1  is  returned, indicating that the pattern matches only at the start
//	//        of a subject string or after any newline within the  string.  Otherwise
//	//        -2 is returned. For anchored patterns, -2 is returned.
//	//
//	//          PCRE_INFO_FIRSTCHARACTER
//	//
//	//        Return  the  value  of  the  first data unit (non-UTF character) of any
//	//        matched string in  the  situation  where  PCRE_INFO_FIRSTCHARACTERFLAGS
//	//        returns  1;  otherwise return 0. The fourth argument should point to an
//	//        uint_t variable.
//	//
//	//        In the 8-bit library, the value is always less than 256. In the  16-bit
//	//        library  the value can be up to 0xffff. In the 32-bit library in UTF-32
//	//        mode the value can be up to 0x10ffff, and up  to  0xffffffff  when  not
//	//        using UTF-32 mode.
//	//
//	//          PCRE_INFO_FIRSTCHARACTERFLAGS
//	//
//	//        Return information about the first data unit of any matched string, for
//	//        a non-anchored pattern. The fourth argument  should  point  to  an  int
//	//        variable.
//	//
//	//        If  there  is  a  fixed first value, for example, the letter "c" from a
//	//        pattern such as (cat|cow|coyote), 1  is  returned,  and  the  character
//	//        value  can  be retrieved using PCRE_INFO_FIRSTCHARACTER. If there is no
//	//        fixed first value, and if either
//	//
//	//        (a) the pattern was compiled with the PCRE_MULTILINE option, and  every
//	//        branch starts with "^", or
//	//
//	//        (b) every branch of the pattern starts with ".*" and PCRE_DOTALL is not
//	//        set (if it were set, the pattern would be anchored),
//	//
//	//        2 is returned, indicating that the pattern matches only at the start of
//	//        a subject string or after any newline within the string. Otherwise 0 is
//	//        returned. For anchored patterns, 0 is returned.
//	//
//	//          PCRE_INFO_FIRSTTABLE
//	//
//	//        If the pattern was studied, and this resulted in the construction of  a
//	//        256-bit  table indicating a fixed set of values for the first data unit
//	//        in any matching string, a pointer to the table is  returned.  Otherwise
//	//        NULL  is returned. The fourth argument should point to an unsigned char
//	//        * variable.
//	//
//	//          PCRE_INFO_HASCRORLF
//	//
//	//        Return 1 if the pattern contains any explicit  matches  for  CR  or  LF
//	//        characters,  otherwise  0.  The  fourth argument should point to an int
//	//        variable. An explicit match is either a literal CR or LF character,  or
//	//        \r or \n.
//	//
//	//          PCRE_INFO_JCHANGED
//	//
//	//        Return  1  if  the (?J) or (?-J) option setting is used in the pattern,
//	//        otherwise 0. The fourth argument should point to an int variable.  (?J)
//	//        and (?-J) set and unset the local PCRE_DUPNAMES option, respectively.
//	//
//	//          PCRE_INFO_JIT
//	//
//	//        Return  1  if  the pattern was studied with one of the JIT options, and
//	//        just-in-time compiling was successful. The fourth argument should point
//	//        to  an  int variable. A return value of 0 means that JIT support is not
//	//        available in this version of PCRE, or that the pattern was not  studied
//	//        with  a JIT option, or that the JIT compiler could not handle this par-
//	//        ticular pattern. See the pcrejit documentation for details of what  can
//	//        and cannot be handled.
//	//
//	//          PCRE_INFO_JITSIZE
//	//
//	//        If  the  pattern was successfully studied with a JIT option, return the
//	//        size of the JIT compiled code, otherwise return zero. The fourth  argu-
//	//        ment should point to a size_t variable.
//	//
//	//          PCRE_INFO_LASTLITERAL
//	//
//	//        Return  the value of the rightmost literal data unit that must exist in
//	//        any matched string, other than at its start, if such a value  has  been
//	//        recorded. The fourth argument should point to an int variable. If there
//	//        is no such value, -1 is returned. For anchored patterns, a last literal
//	//        value  is recorded only if it follows something of variable length. For
//	//        example, for the pattern /^a\d+z\d+/ the returned value is "z", but for
//	//        /^a\dz\d/ the returned value is -1.
//	//
//	//        Since  for  the 32-bit library using the non-UTF-32 mode, this function
//	//        is unable to return the full 32-bit range of characters, this value  is
//	//        deprecated;     instead     the     PCRE_INFO_REQUIREDCHARFLAGS     and
//	//        PCRE_INFO_REQUIREDCHAR values should be used.
//	//
//	//          PCRE_INFO_MATCH_EMPTY
//	//
//	//        Return 1 if the pattern can match an empty  string,  otherwise  0.  The
//	//        fourth argument should point to an int variable.
//	//
//	//          PCRE_INFO_MATCHLIMIT
//	//
//	//        If  the  pattern  set  a  match  limit by including an item of the form
//	//        (*LIMIT_MATCH=nnnn) at the start, the value  is  returned.  The  fourth
//	//        argument  should  point to an unsigned 32-bit integer. If no such value
//	//        has  been  set,  the  call  to  pcre_fullinfo()   returns   the   error
//	//        PCRE_ERROR_UNSET.
//	//
//	//          PCRE_INFO_MAXLOOKBEHIND
//	//
//	//        Return  the  number  of  characters  (NB not data units) in the longest
//	//        lookbehind assertion in the pattern. This information  is  useful  when
//	//        doing  multi-segment  matching  using  the partial matching facilities.
//	//        Note that the simple assertions \b and \B require a one-character look-
//	//        behind.  \A  also  registers a one-character lookbehind, though it does
//	//        not actually inspect the previous character. This is to ensure that  at
//	//        least one character from the old segment is retained when a new segment
//	//        is processed. Otherwise, if there are no lookbehinds in the pattern, \A
//	//        might match incorrectly at the start of a new segment.
//	//
//	//          PCRE_INFO_MINLENGTH
//	//
//	//        If  the  pattern  was studied and a minimum length for matching subject
//	//        strings was computed, its value is  returned.  Otherwise  the  returned
//	//        value is -1. The value is a number of characters, which in UTF mode may
//	//        be different from the number of data units. The fourth argument  should
//	//        point  to an int variable. A non-negative value is a lower bound to the
//	//        length of any matching string. There may not be  any  strings  of  that
//	//        length  that  do actually match, but every string that does match is at
//	//        least that long.
//	//
//	//          PCRE_INFO_NAMECOUNT
//	//          PCRE_INFO_NAMEENTRYSIZE
//	//          PCRE_INFO_NAMETABLE
//	//
//	//        PCRE supports the use of named as well as numbered capturing  parenthe-
//	//        ses.  The names are just an additional way of identifying the parenthe-
//	//        ses, which still acquire numbers. Several convenience functions such as
//	//        pcre_get_named_substring()  are  provided  for extracting captured sub-
//	//        strings by name. It is also possible to extract the data  directly,  by
//	//        first  converting  the  name to a number in order to access the correct
//	//        pointers in the output vector (described with pcre_exec() below). To do
//	//        the  conversion,  you  need  to  use  the  name-to-number map, which is
//	//        described by these three values.
//	//
//	//        The map consists of a number of fixed-size entries. PCRE_INFO_NAMECOUNT
//	//        gives the number of entries, and PCRE_INFO_NAMEENTRYSIZE gives the size
//	//        of each entry; both of these  return  an  int  value.  The  entry  size
//	//        depends  on the length of the longest name. PCRE_INFO_NAMETABLE returns
//	//        a pointer to the first entry of the table. This is a pointer to char in
//	//        the 8-bit library, where the first two bytes of each entry are the num-
//	//        ber of the capturing parenthesis, most significant byte first.  In  the
//	//        16-bit  library,  the pointer points to 16-bit data units, the first of
//	//        which contains the parenthesis  number.  In  the  32-bit  library,  the
//	//        pointer  points  to  32-bit data units, the first of which contains the
//	//        parenthesis number. The rest of the entry is  the  corresponding  name,
//	//        zero terminated.
//	//
//	//        The  names are in alphabetical order. If (?| is used to create multiple
//	//        groups with the same number, as described in the section  on  duplicate
//	//        subpattern numbers in the pcrepattern page, the groups may be given the
//	//        same name, but there is only one entry in the  table.  Different  names
//	//        for  groups  of the same number are not permitted.  Duplicate names for
//	//        subpatterns with different numbers are permitted, but only if PCRE_DUP-
//	//        NAMES  is set. They appear in the table in the order in which they were
//	//        found in the pattern. In the absence  of  (?|  this  is  the  order  of
//	//        increasing  number;  when  (?| is used this is not necessarily the case
//	//        because later subpatterns may have lower numbers.
//	//
//	//        As a simple example of the name/number table,  consider  the  following
//	//        pattern after compilation by the 8-bit library (assume PCRE_EXTENDED is
//	//        set, so white space - including newlines - is ignored):
//	//
//	//          (?<date> (?<year>(\d\d)?\d\d) -
//	//          (?<month>\d\d) - (?<day>\d\d) )
//	//
//	//        There are four named subpatterns, so the table has  four  entries,  and
//	//        each  entry  in the table is eight bytes long. The table is as follows,
//	//        with non-printing bytes shows in hexadecimal, and undefined bytes shown
//	//        as ??:
//	//
//	//          00 01 d  a  t  e  00 ??
//	//          00 05 d  a  y  00 ?? ??
//	//          00 04 m  o  n  t  h  00
//	//          00 02 y  e  a  r  00 ??
//	//
//	//        When  writing  code  to  extract  data from named subpatterns using the
//	//        name-to-number map, remember that the length of the entries  is  likely
//	//        to be different for each compiled pattern.
//	//
//	//          PCRE_INFO_OKPARTIAL
//	//
//	//        Return  1  if  the  pattern  can  be  used  for  partial  matching with
//	//        pcre_exec(), otherwise 0. The fourth argument should point  to  an  int
//	//        variable.  From  release  8.00,  this  always  returns  1,  because the
//	//        restrictions that previously applied  to  partial  matching  have  been
//	//        lifted.  The  pcrepartial documentation gives details of partial match-
//	//        ing.
//	//
//	//          PCRE_INFO_OPTIONS
//	//
//	//        Return a copy of the options with which the pattern was  compiled.  The
//	//        fourth  argument  should  point to an unsigned long int variable. These
//	//        option bits are those specified in the call to pcre_compile(), modified
//	//        by any top-level option settings at the start of the pattern itself. In
//	//        other words, they are the options that will be in force  when  matching
//	//        starts.  For  example, if the pattern /(?im)abc(?-i)d/ is compiled with
//	//        the PCRE_EXTENDED option, the result is PCRE_CASELESS,  PCRE_MULTILINE,
//	//        and PCRE_EXTENDED.
//	//
//	//        A  pattern  is  automatically  anchored by PCRE if all of its top-level
//	//        alternatives begin with one of the following:
//	//
//	//          ^     unless PCRE_MULTILINE is set
//	//          \A    always
//	//          \G    always
//	//          .*    if PCRE_DOTALL is set and there are no back
//	//                  references to the subpattern in which .* appears
//	//
//	//        For such patterns, the PCRE_ANCHORED bit is set in the options returned
//	//        by pcre_fullinfo().
//	//
//	//          PCRE_INFO_RECURSIONLIMIT
//	//
//	//        If  the  pattern set a recursion limit by including an item of the form
//	//        (*LIMIT_RECURSION=nnnn) at the start, the value is returned. The fourth
//	//        argument  should  point to an unsigned 32-bit integer. If no such value
//	//        has  been  set,  the  call  to  pcre_fullinfo()   returns   the   error
//	//        PCRE_ERROR_UNSET.
//	//
//	//          PCRE_INFO_SIZE
//	//
//	//        Return  the  size  of  the  compiled  pattern  in  bytes (for all three
//	//        libraries). The fourth argument should point to a size_t variable. This
//	//        value  does not include the size of the pcre structure that is returned
//	//        by pcre_compile().  The  value  that  is  passed  as  the  argument  to
//	//        pcre_malloc()  when  pcre_compile() is getting memory in which to place
//	//        the compiled data is the value returned by this option plus the size of
//	//        the  pcre  structure. Studying a compiled pattern, with or without JIT,
//	//        does not alter the value returned by this option.
//	//
//	//          PCRE_INFO_STUDYSIZE
//	//
//	//        Return the size in bytes (for all three libraries) of  the  data  block
//	//        pointed to by the study_data field in a pcre_extra block. If pcre_extra
//	//        is NULL, or there is no study data, zero is returned. The fourth  argu-
//	//        ment  should point to a size_t variable. The study_data field is set by
//	//        pcre_study() to record information that will speed up matching (see the
//	//        section  entitled  "Studying  a  pattern"  above).  The  format  of the
//	//        study_data block is private, but its length is made available via  this
//	//        option  so  that  it  can be saved and restored (see the pcreprecompile
//	//        documentation for details).
//	//
//	//          PCRE_INFO_REQUIREDCHARFLAGS
//	//
//	//        Returns 1 if there is a rightmost literal data unit that must exist  in
//	//        any matched string, other than at its start. The fourth argument should
//	//        point to an int variable. If there is no such value, 0 is returned.  If
//	//        returning  1,  the  character  value  itself  can  be  retrieved  using
//	//        PCRE_INFO_REQUIREDCHAR.
//	//
//	//        For anchored patterns, a last literal value is recorded only if it fol-
//	//        lows  something  of  variable  length.  For  example,  for  the pattern
//	//        /^a\d+z\d+/  the   returned   value   1   (with   "z"   returned   from
//	//        PCRE_INFO_REQUIREDCHAR), but for /^a\dz\d/ the returned value is 0.
//	//
//	//          PCRE_INFO_REQUIREDCHAR
//	//
//	//        Return  the value of the rightmost literal data unit that must exist in
//	//        any matched string, other than at its start, if such a value  has  been
//	//        recorded.  The fourth argument should point to an uint32_t variable. If
//	//        there is no such value, 0 is returned.

func (re *Regexp) error(rc int32) error {
	if err, ok := pcreErrors[rc]; ok {
		return err
	}

	return fmt.Errorf("unknown error code: %v", rc)
}

func (re *Regexp) Fullinfo( /*TODO extra_data uintptr, */ what int32) (int32, error) {
	//        int pcre_fullinfo(const pcre *code, const pcre_extra *extra,
	//             int what, void *where);
	switch what {
	case pcre.DPCRE_INFO_CAPTURECOUNT:
		o := mustMalloc(re.tls, unsafe.Sizeof(int32(0)))

		defer crt.Xfree(re.tls, o)

		rc := pcre.Xpcre_fullinfo(re.tls, re.re, 0 /*TODO extra*/, what, o)
		if rc < 0 {
			panic(rc) //TODO
		}

		return *(*int32)(unsafe.Pointer(uintptr(o))), nil
	default:
		return 0, fmt.Errorf("%T.Fullinfo: unsupported 'what': %v", re, what)
	}
}

func (re *Regexp) FindIndex(b []byte, options Option) (loc []int32, err error) {
	ng, err := re.Fullinfo(pcre.DPCRE_INFO_CAPTURECOUNT)
	if err != nil {
		return nil, err
	}

	out := make([]int32, (ng+1)*3)
	n, err := re.Exec(b, options, out) //TODO is there an option like 'ignore submatches'?
	if err != nil {
		return nil, err
	}

	if n == 0 || out[0] == 0 && out[1] == 0 {
		return nil, nil
	}

	return out[:2:2], nil
}

func (re *Regexp) ReplaceAll(b, repl []byte, options Option) (r []byte, err error) {
	ng, err := re.Fullinfo(pcre.DPCRE_INFO_CAPTURECOUNT)
	if err != nil {
		return nil, err
	}

	out := make([]int32, (ng+1)*3)
	for len(b) != 0 {
		n, err := re.Exec(b, options, out) //TODO is there an option like 'ignore submatches'?
		if err != nil {
			return nil, err
		}

		if n == 0 {
			break
		}

		first := out[0]
		next := out[1]
		// abc<first>mno<next>xyz
		r = append(append(r, b[:first]...), repl...)
		b = b[next:]
	}
	return append(r, b...), nil
}

//	// ERROR MESSAGES
//	//
//	//        The regerror() function maps a non-zero errorcode from either regcomp()
//	//        or regexec() to a printable message. If preg is  not  NULL,  the  error
//	//        should have arisen from the use of that structure. A message terminated
//	//        by a binary zero is placed  in  errbuf.  The  length  of  the  message,
//	//        including  the  zero, is limited to errbuf_size. The yield of the func-
//	//        tion is the size of buffer needed to hold the whole message.
//
//	//TODO func (re T) Error(rc int32) error {
//	//TODO 	if rc < 0 {
//	//TODO 		return fmt.Errorf("unknown error code: %v", rc)
//	//TODO 	}
//	//TODO
//	//TODO 	var buf [256]byte
//	//TODO 	r := pcre.Xregerror(0, rc, re.uintptr, uintptr(unsafe.Pointer(&buf[0])), crt.SizeT(len(buf)-1))
//	//TODO 	s := string(buf[:r-1])
//	//TODO 	if s == "" {
//	//TODO 		return nil
//	//TODO 	}
//	//TODO
//	//TODO 	return errors.New(s)
//	//TODO }
