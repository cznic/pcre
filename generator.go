// Copyright 2018 The pcre Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE file.

// +build none

package main

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

var (
	downloads = []struct {
		dir, url string
		sz       int
		dev      bool
	}{
		{pcreDir, "https://ftp.pcre.org/pub/pcre/pcre-8.44.tar.gz", 2000, false},
	}

	pcreDir = filepath.FromSlash("testdata/pcre-8.44")
)

func main() {
	log.SetFlags(log.Flags() | log.Lshortfile)
	if err := main1(); err != nil {
		log.Fatal(err)
	}
}

func main1() error {
	download()
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		return err
	}

	defer os.RemoveAll(dir)

	if err := make(pcreDir); err != nil { //TODO split config && make test
		return err
	}

	if err := makePackage(
		"internal/pcre",
		"pcre_compile.c",
		"pcre_globals.c",
		"pcre_tables.c",
		"pcre_newline.c",
		"pcre_chartables.c",
		"pcre_exec.c",
		"pcre_fullinfo.c",
	); err != nil {
		return err
	}

	//TODO if err := makePackage("internal/pcretest", n["pcretest-pcretest.o"], m["pcre"], m["pcre16"], m["pcre32"], n["pcretest-pcre_printint.o"], n["pcretest-pcre16_printint.o"], n["pcretest-pcre32_printint.o"]); err != nil {
	//TODO 	return err
	//TODO }

	return nil
}

func makePackage(pkgDir string, obj ...string) error {
	pkgDir, err := filepath.Abs(pkgDir)
	if err != nil {
		return err
	}

	if err := os.MkdirAll(pkgDir, 0770); err != nil {
		return err
	}

	wd, err := os.Getwd()
	if err != nil {
		return err
	}

	defer func() {
		if e := os.Chdir(wd); e != nil && err == nil {
			err = e
		}
	}()

	if err = os.Chdir(pcreDir); err != nil {
		return err
	}

	out := filepath.Join(pkgDir, fmt.Sprintf("%pcre_%s_%s.go", runtime.GOOS, runtime.GOARCH))
	args := []string{
		"-DHAVE_CONFIG_H",
		"-I.",
		"-ccgo-long-double-is-double",
		"-o", out,
	}
	args = append(args, obj...)
	cmd := exec.Command("ccgo", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func make(dir string) (err error) {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	defer func() {
		if e := os.Chdir(wd); e != nil && err == nil {
			err = e
		}
	}()
	if err = os.Chdir(dir); err != nil {
		return err
	}
	if err := ioutil.WriteFile("mk.sh", []byte(`
make clean
make distclean
./configure 2>&1 | tee log-test
make 2>&1 | tee -a log-test
make test 2>&1 | tee -a log-test
grep --color=always -ni FAIL log-test
`), 0644); err != nil {
		return err
	}

	defer os.Remove("mk.sh")

	cmd := exec.Command("sh", "mk.sh")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

func untar(dir string, r io.Reader) error {
	gr, err := gzip.NewReader(bufio.NewReader(r))
	if err != nil {
		return err
	}

	tr := tar.NewReader(gr)
	for {
		hdr, err := tr.Next()
		if err != nil {
			if err != io.EOF {
				return err
			}

			return nil
		}

		switch hdr.Typeflag {
		case tar.TypeDir:
			if err = os.MkdirAll(filepath.Join(dir, hdr.Name), 0770); err != nil {
				return err
			}
		case tar.TypeReg, tar.TypeRegA:
			fn := filepath.Join(dir, hdr.Name)
			f, err := os.OpenFile(fn, os.O_CREATE|os.O_WRONLY, os.FileMode(hdr.Mode))
			if err != nil {
				return err
			}

			w := bufio.NewWriter(f)
			if _, err = io.Copy(w, tr); err != nil {
				return err
			}

			if err := w.Flush(); err != nil {
				return err
			}

			if err := f.Close(); err != nil {
				return err
			}

			if err := os.Chtimes(fn, hdr.AccessTime, hdr.ModTime); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unexpected tar header typeflag %#02x", hdr.Typeflag)
		}
	}
}

func download() {
	tmp, err := ioutil.TempDir("", "")
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		return
	}

	defer os.RemoveAll(tmp)

	for _, v := range downloads {
		dir := filepath.FromSlash(v.dir)
		root := filepath.Dir(v.dir)
		fi, err := os.Stat(dir)
		switch {
		case err == nil:
			if !fi.IsDir() {
				fmt.Fprintf(os.Stderr, "expected %s to be a directory\n", dir)
			}
			continue
		default:
			if !os.IsNotExist(err) {
				fmt.Fprintf(os.Stderr, "%s", err)
				continue
			}
		}

		if err := func() error {
			fmt.Printf("Downloading %v MB from %s\n", float64(v.sz)/1000, v.url)
			resp, err := http.Get(v.url)
			if err != nil {
				return err
			}

			defer resp.Body.Close()

			base := filepath.Base(v.url)
			name := filepath.Join(tmp, base)
			f, err := os.Create(name)
			if err != nil {
				return err
			}

			defer os.Remove(name)

			if _, err := io.Copy(f, resp.Body); err != nil {
				return err
			}

			if _, err := f.Seek(0, io.SeekStart); err != nil {
				return err
			}

			switch {
			case strings.HasSuffix(base, ".tar.gz"):
				return untar(root, bufio.NewReader(f))
			}
			panic("internal error") //TODOOK
		}(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
	}
}
