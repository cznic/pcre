# Perl Compatible Regular Expressions

pcre-8.42 without CGo. Handles UTF{8,16,32} and Unicode properties.

### Supported operating systems and architectures

- linux/amd64
- linux/386

More OS/arch support is planned.

### Installation

To install or update pcre

     $ go get [-u] modernc.org/pcre

Documentation: [godoc.org/modernc.org/pcre](http://godoc.org/modernc.org/pcre)

